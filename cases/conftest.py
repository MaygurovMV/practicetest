"""Модуль описывающий конфигурация длс запуска тестов pytest.

Содержит хук функции.
"""

import importlib
from typing import List

from matplotlib.pyplot import flag
from main import findMainWindow
import os
import re
import importlib.util
from inspect import getmembers, isfunction
import core
import pytest
import subprocess


# Хуки ------------------------------------------------------------------------
@pytest.hookimpl(hookwrapper=True)
def pytest_collection_modifyitems(items):
    """
    Хук функция для захвата списка доступных для запуска тестов.

    Args:
        items : Список тестов
    """
    yield
    # will execute as early as possible
    core.cases_list = [item.name for item in items]


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport():
    """Хук функция для захвата результата одного теста."""
    outcome = yield
    result = outcome.get_result()
    if result.when == 'call':
        core.test_result.append(result)

        window = findMainWindow()
        if window:
            window.test_graphic_update()


# Фикстуры -----------------------------------------------
@pytest.fixture(scope="module")
def get_project_folder_name() -> str:
    """Фикстура для получения имени папки проекта."""
    return os.path.basename(core.PATH_TO_CODE_DIR)


@pytest.fixture(scope="module")
def get_project_dirs(get_project_folder_name) -> List[str]:
    """Фикстура для получения списка путей к директориям."""
    dir_files = os.listdir(core.PATH_TO_CODE_DIR)
    full_paths = map(lambda name: os.path.join(core.PATH_TO_CODE_DIR, name),
                     dir_files)
    dirs = []
    for file in full_paths:
        if os.path.isdir(file):
            dirs.append(file)
    return dirs


@pytest.fixture(scope="module")
def get_project_packages_paths(get_project_dirs):
    dirs = get_project_dirs
    packages_paths = []
    for dir in dirs:
        if os.path.exists(os.path.join(dir, "__init__.py")):
            packages_paths.append(dir)
    return packages_paths


@pytest.fixture(scope="module")
def get_modules_in_package_path(get_project_packages_paths) -> List[str]:
    """Фикстура для получения списка путей к модулям в пакетах."""
    packages_paths = get_project_packages_paths
    modules_paths = []
    for package_path in packages_paths:
        dir_files = os.listdir(package_path)
        full_paths = map(lambda name: os.path.join(package_path, name),
                         dir_files)
        for file_path in full_paths:
            if os.path.isfile(file_path):
                _, ext = os.path.splitext(file_path)
                file_name = os.path.basename(file_path)
                if ext == ".py" and not ("__" in file_name):
                    modules_paths.append(file_path)
    return modules_paths


@pytest.fixture(scope="module")
def odata():
    """Фикстура для работы с выходными данными."""
    yield
    with open("output.txt") as output:
        output_data = output.read()
    if not os.path.exists(f"{core.PATH_TO_TEST_CASES}\\{core.CODE_DIR}"):
        os.makedirs(f"{core.PATH_TO_TEST_CASES}\\{core.CODE_DIR}")
    os.replace("input.txt",
               f"{core.PATH_TO_TEST_CASES}\\{core.CODE_DIR}\\input.txt")
    os.replace("output.txt",
               f"{core.PATH_TO_TEST_CASES}\\{core.CODE_DIR}\\output.txt")
    os.replace("fig.png",
               f"{core.PATH_TO_TEST_CASES}\\{core.CODE_DIR}\\fig.png")


@pytest.fixture(scope="module")
def subprocess_() -> subprocess.CompletedProcess:
    """
    Фикстура создания подпроцесса программы.

    Returns:
        CompletedProcess[str]: Завершившийся процесс.
    """
    script = b''
    res = subprocess.run(
        f'python "{core.PATH_TO_CODE_DIR}\\{core.CODE_FILE_NAME}"',
        input=script,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    return res


# Тесты----------------------------------------------------
def test_project_folder_name(get_project_folder_name):
    """Тест правильного имени проекта."""
    assert re.match(r"(\d{1}|\d{2}) (\w+) (\w+)", get_project_folder_name),\
        "Имя должно соответствовать шаблону <Вариант> <Фамилия> <Группа>"


def test_main_name():
    """Тест правильного имени исполняемого файла."""
    assert core.CODE_FILE_NAME == "main.py",\
        "Программа должна завершиться корректно - с кодом 0"


def test_has_package(get_project_packages_paths):
    """Тест наличия пакета."""
    assert get_project_packages_paths, "В папке проектов не существует пакета"


def test_package_name(get_project_packages_paths):
    """Тест корректного имени пакета."""
    for package_path in get_project_packages_paths:
        package_name = os.path.basename(package_path)
        if package_name == "package1":
            assert True
            return True
    assert False, 'Некорректное имя пакета. Имя должно быть "package1".\n' +\
        f'Текущее имя: "{package_name}"'


def test_has_modules(get_modules_in_package_path):
    """Тест наличия модулей."""
    assert get_modules_in_package_path, "Проект не имеет модулей внутри пакета"


modules_names = ["input_module", "output_module", "func_module", "plot_module"]


@pytest.mark.parametrize(
    "module_name",
    modules_names
)
def test_has_module(get_modules_in_package_path, module_name):
    """Тест наличия модуля входных данных."""
    modules = tuple(map(
        lambda x: os.path.splitext(os.path.split(x)[1])[0],
        get_modules_in_package_path))
    assert f"{module_name}" in modules, 'Некорректное имя модулей.\n' +\
        f'Имя должно быть из "{modules_names}". Получено:"{module_name}"'


def _import_module(module_path):
    module_name = os.path.splitext(os.path.split(module_path)[1])[0]
    spec = importlib.util.spec_from_file_location(
        module_name,
        module_path)
    module_var = importlib.util.module_from_spec(spec)
    try:
        spec.loader.exec_module(module_var)
    except ImportError:
        # Проброс ошибки импорта, т.к. необходимо лишь извлечь docstring
        pass
    return module_var


def test_has_modules_docstring(get_modules_in_package_path):
    """Тест наличия docstring в модулях проекта."""
    # Добавление исполняемого файла в список проверяемых модулей
    exec_path = os.path.join(core.PATH_TO_CODE_DIR, core.CODE_FILE_NAME)
    get_modules_in_package_path.append(exec_path)

    for module_path in get_modules_in_package_path:
        module_var = _import_module(module_path)
        assert module_var.__doc__,\
            f'Docstring модуля "{module_var.__name__}" не найден'

        functions_list = getmembers(module_var, isfunction)
        for function_name, function in functions_list:
            assert function.__doc__,\
                f'Docstring функции "{function_name}" не найден'


def test_has_fn_type_hint(get_modules_in_package_path):
    """Тест наличия Type hint в функциях модулей проекта."""
    for module_path in get_modules_in_package_path:
        module_var = _import_module(module_path)
        functions_list = getmembers(module_var, isfunction)
        for function_name, function in functions_list:
            for external_mod_name in ("scipy", "numpy", "matplotlib"):
                if external_mod_name in function.__module__:
                    break
            else:
                assert function.__annotations__,\
                    f'Type hint функции "{function_name}" ' +\
                    f'модуля "{module_var.__name__}" не найден'


def test_returncode(iodata, subprocess_):
    """Тест успешности завершения программы."""
    assert subprocess_.returncode == 0
