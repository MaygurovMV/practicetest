"""Тестовая программа программы калькулятор.

@author Maigurov M.V.
@date: 2.05.22
"""
import pytest
import subprocess
from typing import Optional
import core


@pytest.fixture()
def process(request) -> Optional[subprocess.CompletedProcess]:
    """
    Фикстура запуска тестируемой программы.

    Args:
        request : Фикстура запроса, необходима для запроса входных данных теста
            из метки

    Returns:
        Optional[]: _description_
    """
    marker = request.node.get_closest_marker("input")
    if marker is None:
        data = None
    else:
        data = marker.args[0]
        A, SIGN, B = data.split()
        return subprocess.run(
            f'python "{core.PATH_TO_CODE_DIR}\\{core.CODE_FILE_NAME}"',
            input=bytes(f"{A}\n{SIGN}\n{B}\n", "cp1251"),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
    return data


@pytest.mark.input("2 + 3")
def test_returncode(process):
    """
    Тест на завершение программы без ошибок.

    Errors:
        AssertationError: Программа завершилась с ошибкой
    """
    assert process.returncode == 0


@pytest.mark.input("2 + 3")
def test_input_1(process):
    """
    Тест на правильность запроса первого числа от пользователя.

    Errors:
        AssertationError: Запрос первого числа сделан неправильно.
    """
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, a, sign, b, eq, res =\
        bytes.decode(process.stdout, "cp1251").split()
    assert " ".join([input11, input12, input13]) == "Введите первое число:"


@pytest.mark.input("2 + 3")
def test_input_2(process):
    """
    Тест на правильность запроса второго числа от пользователя.

    Errors:
        AssertationError: Запрос второго числа сделан неправильно.
    """
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, a, sign, b, eq, res =\
        bytes.decode(process.stdout, "cp1251").split()
    assert " ".join([input31, input32, input33]) == "Введите второе число:"


@pytest.mark.input("2 + 3")
def test_input_sign(process):
    """
    Тест на правильность запроса знака от пользователя.

    Errors:
        AssertationError: Запрос первого числа сделан неправильно.
    """
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, a, sign, b, eq, res =\
        bytes.decode(process.stdout, "cp1251").split()
    assert " ".join([input21, input22]) == "Введите знак:"


@pytest.mark.input("2 + 3")
def test_answer(process):
    """
    Тест на правильность запроса формы ответа.

    Errors:
        AssertationError: ЗФорма ответа неправильна.
    """
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, a, sign, b, eq, res =\
        bytes.decode(process.stdout, "cp1251").split()

    assert float(a) == 2
    assert sign == "+"
    assert float(b) == 3
    assert eq == "="


@pytest.mark.input("2 + 3")
def test_plus(process):
    """Тест на сложение."""
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, a, sign, b, eq, res =\
        bytes.decode(process.stdout, "cp1251").split()
    assert float(res) == 2.0 + 3.0


@pytest.mark.input("5 - 3")
def test_minus(process):
    """Тест на вычитание."""
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, a, sign, b, eq, res =\
        bytes.decode(process.stdout, "cp1251").split()
    assert float(res) == 5.0 - 3.0


@pytest.mark.input("2 * 3")
def test_prod(process):
    """Тест на умножение."""
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, a, sign, b, eq, res =\
        bytes.decode(process.stdout, "cp1251").split()
    assert float(res) == 2.0 * 3.0


@pytest.mark.input("3 / 2")
def test_frac(process):
    """Тест на деление."""
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, a, sign, b, eq, res =\
        bytes.decode(process.stdout, "cp1251").split()
    assert float(res) == 3.0 / 2.0


@pytest.mark.input("3 / 0")
def test_frac_zero(process):
    """Тест на деление нуль.

    Errors:
        AssertationError: Не отработано деление на нуль.
    """
    input11, input12, input13, input21, input22, input31, input32, input33,\
        answer, out1, out2, out3 =\
        bytes.decode(process.stdout, "cp1251").split()
    assert answer == "Ошибка:"
    assert " ".join([out1, out2, out3]) == "Деление на нуль"
