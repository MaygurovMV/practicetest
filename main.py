"""
Основной модуль программы, выполняющий тесты программ студентов.

@author: Майгуров М.В.
@date: 28.04.22
@license: MIT
"""
import sys
from typing import Optional
import os
from PySide2 import __file__
from PySide2.QtWidgets import (
    QApplication, QMainWindow, QStyle, QFileDialog, QListWidgetItem)
from PySide2.QtCore import Qt
import pytest
import core
from gui.gui import Ui_MainWindow


class MainWindow(QMainWindow):
    """Класс основного окна."""

    def __init__(self) -> None:
        """Конструктор главного окна."""
        super().__init__()
        self.is_test_done = False
        self.is_test_processing = False
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowIcon(
            self.style().standardIcon(QStyle.SP_FileDialogContentsView))

        self.ui.label_3.setText(core.PATH_TO_TEST_CASE)
        self.ui.label_4.setText(
            f"{core.PATH_TO_CODE_DIR}/{core.CODE_FILE_NAME}")
        self.ui.label_6.setText(f"{core.PATH_TO_TEST_CASES}")

        # Установка сигналов и слотов
        self.ui.action.triggered.connect(self.set_test_program_path)
        self.ui.action_2.triggered.connect(self.set_program_path)
        self.ui.action_6.triggered.connect(sys.exit)
        self.ui.toolButton.clicked.connect(self.set_test_program_path)
        self.ui.toolButton_2.clicked.connect(self.set_program_path)
        self.ui.progressBar.setValue(0)
        self.ui.pushButton.clicked.connect(self.start_test)

    def set_test_program_path(self):
        """Слот установки пути к тестовой программе."""
        f_name, _ = QFileDialog.getOpenFileName(
            self, 'Открыть тестовую программу', '.\\')
        if f_name:
            core.PATH_TO_TEST_CASE = f_name
            core.PATH_TO_TEST_CASES = os.path.splitext(f_name)[0]
            self.is_test_done = False
            self.is_test_processing = False
            self.ui.textBrowser.setEnabled(False)
            self.ui.label_3.setText(f"{core.PATH_TO_TEST_CASE}")
            self.ui.label_6.setText(f"{core.PATH_TO_TEST_CASES}")
            self.ui.progressBar.setValue(0)
            self.test_list_update()
            self.ui.textBrowser.clear()

    def set_program_path(self):
        """Слот установки пути к программе."""
        f_name, _ = QFileDialog.getOpenFileName(
            self, 'Открыть программу', '.\\')
        if f_name:
            core.PATH_TO_CODE_DIR, core.CODE_FILE_NAME = os.path.split(f_name)
            self.is_test_done = False
            self.ui.label_4.setText(f_name)
            self.test_list_update()

    def test_graphic_update(self):
        """Обновление графики теста.

        Обновление включает в себя как обновление списка тестов, так и
        обновление прогресс бара.
        """
        self.test_list_update()
        self.ui.progressBar.setValue(
            int(100 * len(core.test_result)/len(core.cases_list)))

    def test_list_update(self):
        """Обновление списка тестов."""
        if core.PATH_TO_TEST_CASE and not self.is_test_processing:
            self.ui.listWidget.clear()
            self.ui.listWidget.addItems(core.collect_test_cases())
        if self.is_test_done:
            for item in core.test_result:
                curres = self.ui.listWidget.findItems(
                        item.head_line, Qt.MatchExactly)[0]
                if item.passed:
                    curres.setIcon(self.style().standardIcon(
                        QStyle.SP_DialogApplyButton))
                else:
                    curres.setIcon(self.style().standardIcon(
                        QStyle.SP_DialogCancelButton))

    def chose_test_item_list(self):
        """Отображение результатов теста."""
        for test_result_item in core.test_result:
            self.ui.listWidget.itemClicked.connect(self.view_test_result)

    def view_test_result(self, test_result_item: QListWidgetItem):
        """
        Функция отображения результатов выбранного теста.

        Args:
            test_result_item (str): Имя выбранного теста
        """
        res = tuple(
            filter(lambda x: x.head_line == test_result_item.text(),
                   core.test_result))[0]
        self.ui.textBrowser.setText(res.longreprtext)

    def start_test(self):
        """Запуск тестирования."""
        self.is_test_done = False
        self.is_test_processing = True
        self.ui.progressBar.setValue(0)
        self.test_list_update()

        core.start()

        self.is_test_done = True
        self.is_test_processing = False
        self.test_list_update()
        self.ui.textBrowser.setEnabled(True)
        self.chose_test_item_list()


def findMainWindow() -> Optional[MainWindow]:
    """
    Поиск главного окна.

    Функция необходима для доступа к объекту windows из другого файла.

    Returns:
        Optional[MainWindow]: Главное.
    """
    app = QApplication.instance()
    if app:
        for widget in app.topLevelWidgets():
            if isinstance(widget, QMainWindow):
                return widget
    return None


if __name__ == '__main__':
    # Установка имени правильного пути к платформе QT
    dirname = os.path.dirname(__file__)
    plugin_path = os.path.join(dirname, 'plugins', 'platforms')
    os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = plugin_path

    app = QApplication(sys.argv)
    window = MainWindow()
    window.test_list_update()
    window.show()

    sys.exit(app.exec_())
