"""Модуль теста для 1 варианта экзаменационного задания."""
import os
import pytest
from conftest import *

INPUT_DATA = "# Пределы интегрирования\n6 -0.9 0.9"
EXPECTED_OUTPUT_DATA = "6 Ответ: -1.235461248114289"


@pytest.fixture(scope="module")
def iodata(odata):
    """
    Фикстура создания и перемещения исходных данных.

    Создает файл входных данных. После выполнения теста перемещает
    результаты в папку теста.
    Returns:
        str: Сообщение о входных данных.
    """
    with open(r"input.txt", 'w') as input:
        input.write(INPUT_DATA)
    yield INPUT_DATA


def test_answer(iodata, subprocess_):
    """Тест правильности ответа."""
    with open("output.txt") as output:
        output_data = output.readline()
        a1, a2, a3 = output_data.split()
        b1, b2, b3 = EXPECTED_OUTPUT_DATA.split()
        assert a1 == b1
        assert a2 == b2
        assert float(a3) == pytest.approx(float(b3), abs=0.1)
    # assert res.stdout
