"""Модуль, содержащий не GUI код программы."""
from typing import List
import os
import pytest

PATH_TO_CODE_DIR: str = r"D:\Code\Пед. практика\Вопросы\0 Майгуров 6о-116А"
CODE_DIR: str = os.path.split(PATH_TO_CODE_DIR)[1]
CODE_FILE_NAME: str = "main.py"
PATH_TO_TEST_CASE: str = r"./cases/test_var0.py"
PATH_TO_TEST_CASES_DATA = r"./cases/test_var0"
PATH_TO_TEST_CASES: str = os.path.split(PATH_TO_TEST_CASE)[0]
METHOD: str = "-s"
cases_list: List[str] = []
test_result: List[pytest.TestReport] = []


def start() -> int:
    """Запуск ядра тестирования."""
    test_result.clear()
    retcode = pytest.main(args=[
        # "-v",
        # TODO уточнить назначение"-s",
        f"{PATH_TO_TEST_CASE}"])
    return retcode


def collect_test_cases():
    """Перечень тестовых случаев."""
    retcode = pytest.main(
        args=[
            # "-v",
            # "--capture=sys"
            # " ",
            "--co",
            f"{PATH_TO_TEST_CASE}"
            # " ",
            # "--junit-xml=junit.xml"
        ]
    )
    return cases_list


if __name__ == '__main__':
    start()
